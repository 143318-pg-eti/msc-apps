name := "wordcount-flink-streaming"

scalaVersion := "2.11.7"

libraryDependencies += "org.apache.flink" %%  "flink-scala" % "1.2.0" % "provided"
libraryDependencies += "org.apache.flink" %% "flink-clients" % "1.2.0" % "provided"
libraryDependencies += "org.apache.flink" %% "flink-streaming-scala" % "1.2.0" % "provided"
libraryDependencies += "org.apache.flink" %% "flink-connector-kafka-0.10" % "1.2.0"

//artifactName := { (sv: ScalaVersion, module: ModuleID, artifact: Artifact) =>
//  artifact.name + "." + artifact.extension
//}
