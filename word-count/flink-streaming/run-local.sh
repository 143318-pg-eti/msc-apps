#!/usr/bin/env bash
set -xuve

echo "Required variables"
echo FLINK_HOME=${FLINK_HOME}
echo INPUT_TOPIC=${INPUT_TOPIC}
echo OUTPUT_TOPIC=${OUTPUT_TOPIC}
echo BOOTSTRAP_SERVERS=${BOOTSTRAP_SERVERS}


SCRIPT_LOC=`dirname $0`

${FLINK_HOME}/bin/start-local.sh &

${FLINK_HOME}/bin/flink run \
    -c Main \
    ${SCRIPT_LOC}/target/scala-2.11/wordcount-flink-streaming-assembly-0.1-SNAPSHOT.jar \
    ${INPUT_TOPIC} ${OUTPUT_TOPIC} ${BOOTSTRAP_SERVERS}

${FLINK_HOME}/bin/stop-local.sh
ps aux | grep flink | grep -v grep | awk '{print $2}' | xargs kill

