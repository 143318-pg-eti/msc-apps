import java.util.{Properties, UUID}

import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.api.windowing.assigners.{TumblingEventTimeWindows, TumblingProcessingTimeWindows}
import org.apache.flink.streaming.api.windowing.time.Time
import org.apache.flink.streaming.connectors.kafka.{FlinkKafkaConsumer010, FlinkKafkaProducer010}
import org.apache.flink.streaming.util.serialization.{SimpleStringSchema, TypeInformationKeyValueSerializationSchema}
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.common.serialization.StringDeserializer

import scala.concurrent.Future


object Main {
  def main(args: Array[String]) {

    // set up the execution environment
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    val inputTopic = args(0)
    val outputTopic = args(1)
    val bootstrapServers = args(2)

    val properties = new Properties()
    properties.setProperty("bootstrap.servers", bootstrapServers)
    properties.setProperty("group.id", s"${UUID.randomUUID().toString}")
    properties.setProperty("auto.offset.reset", "earliest")
    properties.setProperty("enable.auto.commit", "false")

    val stream = env
      .addSource(new FlinkKafkaConsumer010[String](inputTopic, new SimpleStringSchema(), properties))
      .flatMap(str => str.split("[\\s]+").filter(_.nonEmpty))
      .map(word => (word, 1))
      .keyBy(_._1)
      .window(TumblingProcessingTimeWindows.of(Time.seconds(5)))
      .reduce((x1, x2) => (x1._1, x1._2 + x2._2))
      .map(x => s"${x._1} ${x._2}")

    val myProducerConfig = FlinkKafkaProducer010.writeToKafkaWithTimestamps(
      stream.javaStream,
      outputTopic,
      new SimpleStringSchema(),
      properties)

    stream.print()

    import scala.concurrent.ExecutionContext.Implicits.global
    Future {
      Thread.sleep(10000)
      System.exit(0)
    }
    env.execute()
  }

}