name := "word-count"

version in ThisBuild := "0.1.0"

scalaVersion in ThisBuild := "2.11.8"

lazy val mapreduce = project
lazy val `flink-batch` = project
lazy val `spark-core` = project

lazy val `spark-streaming` = project
lazy val `flink-streaming` = project
lazy val `storm-core` = project
lazy val `storm-trident` = project
lazy val `kafka-streams-processors` = project
lazy val `kafka-streams-streams` = project
lazy val `gearpump-dsl` = project
lazy val `gearpump-processor` = project


