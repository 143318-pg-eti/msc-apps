#!/usr/bin/env bash
set -xuve

echo "Required variables"
echo INPUT_TOPIC =${INPUT_TOPIC}
echo OUTPUT_TOPIC =${OUTPUT_TOPIC}


SCRIPT_LOC=`dirname $0`

java -cp ${SCRIPT_LOC}/target/scala-2.11/wordcount-kafka-streams-streams-assembly-0.1-SNAPSHOT.jar Main \
    ${INPUT_TOPIC} ${OUTPUT_TOPIC} ${BOOTSTRAP_SERVERS} &

sleep 4
echo "STOP" | $KAFKA_HOME/bin/kafka-console-producer.sh --broker-list $BOOTSTRAP_SERVERS --topic $INPUT_TOPIC
sleep 3
kill %1


