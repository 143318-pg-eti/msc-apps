import java.util.{Properties, UUID}

import akka.actor.ActorSystem
import org.apache.gearpump.cluster.UserConfig
import org.apache.gearpump.cluster.client.ClientContext
import org.apache.gearpump.cluster.main.ArgumentsParser
import org.apache.gearpump.streaming.dsl.StreamApp
import org.apache.gearpump.streaming.dsl.window.api.{CountWindow, FixedWindow}
import org.apache.gearpump.streaming.kafka.KafkaStoreFactory
import org.apache.gearpump.streaming.kafka.dsl.KafkaDSL
import org.apache.gearpump.streaming.kafka.dsl.KafkaDSL._
import org.apache.gearpump.streaming.kafka.util.KafkaConfig
import org.apache.gearpump.util.AkkaApp

object Main extends AkkaApp with ArgumentsParser {

  override def main(akkaConf: Config, args: Array[String]): Unit = {
    val inputTopic = args(0)
    val outputTopic = args(1)
    val bootstrapServers = args(2)

    implicit val actorSystem = ActorSystem()
    val context = ClientContext()
    val appConfig = UserConfig.empty
    val props = new Properties
    props.put(KafkaConfig.ZOOKEEPER_CONNECT_CONFIG, "localhost:2181")
    props.put(KafkaConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers)
    props.put(KafkaConfig.CHECKPOINT_STORE_NAME_PREFIX_CONFIG, UUID.randomUUID().toString)
    props.put("serializer.class", "kafka.serializer.StringEncoder")
    props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    props.put("auto.offset.reset", "smallest")
    val checkpointStoreFactory = new KafkaStoreFactory(props)

    val app = StreamApp("wordcount_gearpump_dsl", context)
    val stream = KafkaDSL.createAtLeastOnceStream[Array[Byte]](app, inputTopic, checkpointStoreFactory, props)
      .flatMap(line => new String(line).split("[\\s]+").map(_.filter(_.isLetterOrDigit)).filter(_.nonEmpty))
      .map((_, 1))
      .groupByKey()
      .sum
      .map { case (word, count) => s"$word $count".getBytes }
      stream.writeToKafka(outputTopic, props)

    val appId = context.submit(app)
    Thread.sleep(2000)
    context.shutdown(appId)
    context.close()
  }


}
