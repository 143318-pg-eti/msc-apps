#!/usr/bin/env bash
set -xuve

echo "Required variables"
echo GEARPUMP_HOME=${GEARPUMP_HOME}
echo INPUT_TOPIC =${INPUT_TOPIC}
echo OUTPUT_TOPIC =${OUTPUT_TOPIC}
echo BOOTSTRAP_SERVERS =${BOOTSTRAP_SERVERS}

SCRIPT_LOC=`dirname $0`

set +e
ps aux | grep gearpump | awk '{print $2}' | xargs kill -9
${GEARPUMP_HOME}/bin/local -sameprocess true &

${GEARPUMP_HOME}/bin/gear app -jar ${SCRIPT_LOC}/target/scala-2.11/wordcount-gearpump-dsl-assembly-0.1.0.jar \
    Main \
    ${INPUT_TOPIC} ${OUTPUT_TOPIC} ${BOOTSTRAP_SERVERS} 12000 &

sleep 2
echo "STOP" | $KAFKA_HOME/bin/kafka-console-producer.sh --broker-list $BOOTSTRAP_SERVERS --topic $INPUT_TOPIC
sleep 2
kill %2 %1

set -e