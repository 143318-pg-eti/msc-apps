import java.time.Instant
import java.util.{Properties, UUID}
import java.util.concurrent.TimeUnit

import akka.actor.{ActorSystem, Cancellable}
import org.apache.gearpump.Message
import org.apache.gearpump.cluster.UserConfig
import org.apache.gearpump.cluster.client.ClientContext
import org.apache.gearpump.cluster.main.{ArgumentsParser, CLIOption, ParseResult}
import org.apache.gearpump.streaming.{Processor, StreamApplication}
import org.apache.gearpump.streaming.kafka.util.KafkaConfig
import org.apache.gearpump.streaming.kafka.{KafkaSink, KafkaSource, KafkaStoreFactory}
import org.apache.gearpump.streaming.partitioner.{HashPartitioner, Partitioner, ShufflePartitioner}
import org.apache.gearpump.streaming.sink.DataSinkProcessor
import org.apache.gearpump.streaming.source.DataSourceProcessor
import org.apache.gearpump.streaming.task.{Task, TaskContext}
import org.apache.gearpump.util.Graph

import scala.collection.mutable
import scala.concurrent.duration.FiniteDuration

object Main extends App with ArgumentsParser {

  val inputTopic = args(0)
  val outputTopic = args(1)
  val bootstrapServers = args(2)

  implicit val actorSystem = ActorSystem()
  val context = ClientContext()
  val appConfig = UserConfig.empty
  val props = new Properties
  props.put(KafkaConfig.ZOOKEEPER_CONNECT_CONFIG, "localhost:2181")
  props.put(KafkaConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers)
  props.put(KafkaConfig.CHECKPOINT_STORE_NAME_PREFIX_CONFIG, UUID.randomUUID().toString)
  props.put(KafkaConfig.CHECKPOINT_STORE_NAME_PREFIX_CONFIG, UUID.randomUUID().toString)
  props.put("serializer.class", "kafka.serializer.StringEncoder")
  props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
  props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")
  props.put("auto.offset.reset", "smallest")

  val source = new KafkaSource(inputTopic, props)
  source.setCheckpointStore(new KafkaStoreFactory(props))

  val sourceProcessor = DataSourceProcessor(source)
  val sinkProcessor = DataSinkProcessor(new KafkaSink(outputTopic, props))

  val b2sProcessor = Processor[ByteArray2String](1)
  val splitProcessor = Processor[Split](1)
  val sumProcessor = Processor[Sum](1)

  import Graph._

  val app = StreamApplication("wordCount", Graph[Processor[_ <: Task], Partitioner](sourceProcessor ~> b2sProcessor ~> splitProcessor ~> sumProcessor ~> sinkProcessor), UserConfig.empty)
  val appId = context.submit(app)
  context.close()
}
class Split(taskContext : TaskContext, conf: UserConfig) extends Task(taskContext, conf) {
  import taskContext.output
  override def onNext(msg : Message) : Unit = {
    msg.msg.asInstanceOf[String].lines.foreach { line =>
      line.split("[\\s]+").filter(_.nonEmpty).foreach { msg =>
        output(new Message(msg, System.currentTimeMillis()))
      }
    }
  }
}

class Sum (taskContext : TaskContext, conf: UserConfig) extends Task(taskContext, conf) {
  private val map : mutable.HashMap[String, Long] = new mutable.HashMap[String, Long]()

  private var scheduler : Cancellable = null

  override def onStart(startTime : Instant) : Unit = {
    scheduler = taskContext.scheduleOnce(new FiniteDuration(2, TimeUnit.SECONDS))(reportWordCount())
  }

  override def onNext(msg : Message) : Unit = {
    if (null == msg) {
      return
    }
    val current = map.getOrElse(msg.msg.asInstanceOf[String], 0L)
    map.put(msg.msg.asInstanceOf[String], current + 1)
  }

  override def onStop() : Unit = {
    if (scheduler != null) {
      scheduler.cancel()
    }
  }

  def reportWordCount() : Unit = {
    map
      .map{ case (key, value) => s"$key $value" }
      .foreach(x => taskContext.output(new Message(x.getBytes)))
  }
}

class ByteArray2String(taskContext : TaskContext, conf: UserConfig) extends Task(taskContext, conf) {
  override def onNext(msg : Message) : Unit = {
    taskContext.output(new Message(new String(msg.msg.asInstanceOf[Array[Byte]])))
  }
}


