name := "wordcount-gearpump-processor"

version := "0.1.0"

scalaVersion := "2.11.7"

libraryDependencies += "org.apache.gearpump" %% "gearpump-core" % "0.8.2" % "provided"
libraryDependencies += "org.apache.gearpump" %% "gearpump-streaming" % "0.8.2" % "provided"
libraryDependencies += "org.apache.gearpump" %% "gearpump-external-kafka" % "0.8.2"
