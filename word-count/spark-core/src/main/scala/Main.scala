
import org.apache.hadoop.io.{IntWritable, Text}
import org.apache.hadoop.mapred.TextOutputFormat
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf

object Main {
  def main(args: Array[String]) {
    val conf = new SparkConf().setAppName("word-count spark-batch")
    val sc = new SparkContext(conf)

    sc.textFile(args(0))
      .flatMap(_.split(' '))
      .map(t => (t,1))
      .reduceByKey(_+_)
//      .sortBy(_._2, ascending = false, 1)
      .map{ case (k,v) => (new Text(k), new IntWritable(v))}
      .saveAsHadoopFile[TextOutputFormat[Text, IntWritable]](args(1))

    sc.stop()
  }
}