#!/usr/bin/env bash
set -xuve

echo "Required variables"
echo SPARK_HOME=${SPARK_HOME}
echo INPUT_PATH=${INPUT_PATH}
echo OUTPUT_PATH=${OUTPUT_PATH}
echo SPARK_MASTER=${SPARK_MASTER}


SCRIPT_LOC=`dirname $0`

${SPARK_HOME}/bin/spark-submit \
    --master "$SPARK_MASTER" \
    --class Main \
    ${SCRIPT_LOC}/target/scala-2.11/wordcount-spark-batch_2.11-0.1.0.jar \
    ${INPUT_PATH} ${OUTPUT_PATH}
