-- Query for https://data.stackexchange.com/stackoverflow/query/new
-- Tag: Tag name (lowercase) or 'NA' (uppercase) "Enter a tag name or uppercase NA or a single space"
DECLARE @Tag varchar(255)  = ##Tag:string##;
WITH QuestStatsByMonth  AS (
  SELECT
    DATEADD (month, DATEDIFF (month, 0, q.CreationDate), 0)  AS [Month],
    SUM (q.Score)               AS TotalScore,
    COUNT (q.Id)                AS NumQuests
  FROM Posts q
    INNER JOIN  PostTags pt ON q.Id = pt.PostId
    INNER JOIN  Tags t ON t.Id = pt.TagId
  WHERE       
    q.PostTypeId    = 1
    AND t.TagName = @Tag
  GROUP BY
    DATEADD (month, DATEDIFF (month, 0, q.CreationDate), 0)
)
SELECT
    q.[Month],
    SUM (h.TotalScore) AS Score,
    SUM (h.NumQuests) AS QCount

FROM QuestStatsByMonth   q
  GROUP BY q.[Month]
  ORDER BY q.[Month]