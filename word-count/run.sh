#!/bin/bash
set -xuve

function main {
    SCRIPT_LOC=`dirname $0`

    source $SCRIPT_LOC/install-deps.sh

    echo "Required variables"
    echo SPARK_HOME=${SPARK_HOME}
    echo HADOOP_HOME=${HADOOP_HOME}
    echo FLINK_HOME=${FLINK_HOME}

    export JAVA_HOME=/usr/java/jdk1.8.0_73/ # FIXME
    echo JAVA_HOME=${JAVA_HOME}


    OUTPUT_PATH_BASE="$SCRIPT_LOC/output"
    mkdir -p $OUTPUT_PATH_BASE

    export INPUT_PATH=$HADOOP_HOME/LICENSE.txt

    runBatchProjects

    runStreamingProjects
}

function runBatchProjects {
    startKafkaCluster
    batch_projects="mapreduce spark-core flink-batch"
    for project in $batch_projects; do
        export OUTPUT_PATH="$OUTPUT_PATH_BASE/$project"
        rm -rf $OUTPUT_PATH
        (
            echo "Building $project"
            cd $project
            ./build.sh
            echo "Running $project"
            ./run-local.sh
        )
    done
}

function runStreamingProjects {
    streaming_projects="flink-streaming gearpump-dsl gearpump-processor kafka-streams-processors kafka-streams-streams spark-streaming storm-core storm-trident samza"
    startKafkaCluster
    createInputData
    for project in $streaming_projects; do
        export OUTPUT_TOPIC=$project-output
        createTopic $OUTPUT_TOPIC
        (
            echo "Building $project"
            cd $project
            ./build.sh
            echo "Running $project"
            ./run-local.sh
#            export APP_PID=$!
#            sleep 10
#            produceStopMessage
#            sleep 5
#
            $KAFKA_HOME/bin/kafka-console-consumer.sh \
                --bootstrap-server $BOOTSTRAP_SERVERS \
                --from-beginning \
                --timeout-ms 1000 \
                --topic $OUTPUT_TOPIC >"$OUTPUT_PATH_BASE/$project"
        )
    done
    stopKafkaCluster
}


function createTopic {
    set +e
    $KAFKA_HOME/bin/kafka-topics.sh --list --zookeeper $ZOOKEEPER
    $KAFKA_HOME/bin/kafka-topics.sh --create --zookeeper $ZOOKEEPER --topic $1 --partitions 2 --replication-factor 1
    set -e
}

function createInputData {
    export INPUT_TOPIC="wordcount-input"
    createTopic $INPUT_TOPIC
    cat $INPUT_PATH | $KAFKA_HOME/bin/kafka-console-producer.sh --broker-list $BOOTSTRAP_SERVERS --topic $INPUT_TOPIC
}

function produceStopMessage {
    echo "STOP" | $KAFKA_HOME/bin/kafka-console-producer.sh --broker-list $BOOTSTRAP_SERVERS --topic $INPUT_TOPIC
}


function startKafkaCluster {
    echo "STARTING KAFKA"
    set +e
    stopKafkaCluster
    rm -r /tmp/zookeeper
    rm -r /tmp/kafka-logs
    set -e
    $KAFKA_HOME/bin/zookeeper-server-start.sh $KAFKA_HOME/config/zookeeper.properties &>$KAFKA_HOME/zookeeper.log &
    sleep 5
    $KAFKA_HOME/bin/kafka-server-start.sh $KAFKA_HOME/config/server.properties &>$KAFKA_HOME/server.log &
    sleep 5
    export BOOTSTRAP_SERVERS="localhost:9092"
    export ZOOKEEPER="localhost:2181"
}

function stopKafkaCluster {
    echo "STOPING KAFKA"
    $KAFKA_HOME/bin/kafka-server-stop.sh $KAFKA_HOME/config/server.properties
    $KAFKA_HOME/bin/zookeeper-server-stop.sh $KAFKA_HOME/config/zookeeper.properties
    ps aux | grep kafka | awk '{ print $2 }' | xargs kill -9
}

main $@