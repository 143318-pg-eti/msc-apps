#!/usr/bin/env bash
set -xuve

echo "Required variables"
echo HADOOP_HOME=${HADOOP_HOME}
echo INPUT_PATH=${INPUT_PATH}
echo OUTPUT_PATH=${OUTPUT_PATH}


SCRIPT_LOC=`dirname $0`

yes | ${HADOOP_HOME}/bin/hadoop jar ${SCRIPT_LOC}/target/scala-2.11/wordcount-mapreduce.jar ${INPUT_PATH} ${OUTPUT_PATH}


