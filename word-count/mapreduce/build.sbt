name := "wordcount-mapreduce"


libraryDependencies += "org.apache.hadoop" % "hadoop-client" % "2.7.3"

artifactName := { (sv: ScalaVersion, module: ModuleID, artifact: Artifact) =>
  artifact.name + "." + artifact.extension
}