#!/usr/bin/env bash
set -xuve

echo "Required variables"
echo INPUT_TOPIC =${INPUT_TOPIC}
echo OUTPUT_TOPIC =${OUTPUT_TOPIC}
echo BOOTSTRAP_SERVERS =${BOOTSTRAP_SERVERS}


SCRIPT_LOC=`dirname $0`

java -cp ${SCRIPT_LOC}/target/scala-2.11/wordcount-kafka-streams-processors-assembly-0.1-SNAPSHOT.jar Main \
    ${INPUT_TOPIC} ${OUTPUT_TOPIC} ${BOOTSTRAP_SERVERS} &

sleep 2
echo "STOP" | $KAFKA_HOME/bin/kafka-console-producer.sh --broker-list $BOOTSTRAP_SERVERS --topic $INPUT_TOPIC
sleep 2
kill %1


