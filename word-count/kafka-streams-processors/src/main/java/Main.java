import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.processor.*;
import org.apache.kafka.streams.state.KeyValueIterator;
import org.apache.kafka.streams.state.KeyValueStore;
import org.apache.kafka.streams.state.Stores;

import java.util.Properties;
import java.util.UUID;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        TopologyBuilder builder = new TopologyBuilder();

        String inputTopic = args[0];
        String outputTopic = args[1];
        String bootstrapServers = args[2];

        StateStoreSupplier countStore = Stores.create("Counts")
                .withKeys(Serdes.String())
                .withValues(Serdes.Long())
                .inMemory()
                .build();

        builder.addSource("SOURCE", new StringDeserializer(), new StringDeserializer(), inputTopic)
                .addProcessor("PROCESS1", MyProcessor::new, "SOURCE")
                .addStateStore(countStore, "PROCESS1")
                .addSink("SINK1", outputTopic, new StringSerializer(), new StringSerializer(), "PROCESS1");

        Properties settings = new Properties();
        settings.put(StreamsConfig.APPLICATION_ID_CONFIG, UUID.randomUUID().toString());
        settings.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);

        StreamsConfig config = new StreamsConfig(settings);

        KafkaStreams streams = new KafkaStreams(builder, config);
        streams.start();
        Thread.sleep(5000L);
        streams.close();
    }

    public static class MyProcessor implements Processor<String, String> {
        private ProcessorContext context;
        private KeyValueStore<String, Long> kvStore;

        @Override
        @SuppressWarnings("unchecked")
        public void init(ProcessorContext context) {
            // keep the processor context locally because we need it in punctuate() and commit()
            this.context = context;

            this.context.schedule(2000);

            // retrieve the key-value store named "Counts"
            this.kvStore = (KeyValueStore<String, Long>) context.getStateStore("Counts");
        }

        @Override
        public void process(String dummy, String line) {
            String[] words = line.toLowerCase().split("[\\s]+");

            for (String word : words) {
//                System.out.println(word);
                if(!word.isEmpty()) {
                    Long oldValue = this.kvStore.get(word);
                    if (oldValue == null) {
                        this.kvStore.put(word, 1L);
                    } else {
                        this.kvStore.put(word, oldValue + 1L);
                    }
                }
            }
        }

        @Override
        public void punctuate(long timestamp) {
            System.out.println("Start "+timestamp);
            KeyValueIterator<String, Long> iter = this.kvStore.all();

            while (iter.hasNext()) {
                KeyValue<String, Long> entry = iter.next();
                String msg = entry.key + " " + entry.value.toString();
                System.out.println(msg);
                context.forward(entry.key, msg);
            }

            iter.close();
            // commit the current processing progress
            context.commit();
            System.out.println("End "+timestamp);
        }

        @Override
        public void close() {
            // close the key-value store
            this.kvStore.close();
        }
    }
}