#!/usr/bin/env bash


#bin/grid bootstrap

rand=`cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 8 | head -n 1`

mvn \
    -DjobFactory=org.apache.samza.job.local.ThreadJobFactory \
    -DinputTopic=${INPUT_TOPIC} \
    -DoutputTopic=${OUTPUT_TOPIC} \
    -DbootstrapServers=${BOOTSTRAP_SERVERS} \
    -DcountJobName=wordcount-count-$rand \
    -DsplitJobName=wordcount-split-$rand \
    clean package
rm -rf deploy/samza
mkdir -p deploy/samza
tar -xvf ./target/hello-samza-0.12.0-dist.tar.gz -C deploy/samza


deploy/samza/bin/run-job.sh \
    --config-factory=org.apache.samza.config.factories.PropertiesConfigFactory \
    --config-path=file://$PWD/deploy/samza/config/wordcount-count.properties &

sleep 1

deploy/samza/bin/run-job.sh \
    --config-factory=org.apache.samza.config.factories.PropertiesConfigFactory \
    --config-path=file://$PWD/deploy/samza/config/wordcount-split.properties &

sleep 5
kill %1 %2
