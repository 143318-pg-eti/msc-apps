/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import org.apache.samza.config.Config;
import org.apache.samza.system.IncomingMessageEnvelope;
import org.apache.samza.system.OutgoingMessageEnvelope;
import org.apache.samza.system.SystemStream;
import org.apache.samza.task.*;

import java.util.HashMap;
import java.util.Map;

public class WordcountCountTask implements StreamTask, InitableTask, WindowableTask {
    private Map<String, Integer> counts = new HashMap<String, Integer>();
    SystemStream outputStream;


    public void init(Config config, TaskContext context) {
        String outputTopic = config.get("outputTopic");
        outputStream = new SystemStream("kafka", outputTopic);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void process(IncomingMessageEnvelope envelope, MessageCollector collector, TaskCoordinator coordinator) {
        String word = (String) envelope.getMessage();
        Integer oldCount = counts.get(word);
        if (oldCount == null) {
            oldCount = 0;
        }
        counts.put(word, oldCount + 1);
    }

    @Override
    public void window(MessageCollector collector, TaskCoordinator coordinator) {

        for (Map.Entry<String, Integer> entry : counts.entrySet()) {
            String message = entry.getKey() + " " + entry.getValue().toString();
            collector.send(new OutgoingMessageEnvelope(outputStream, message));
        }
        // Reset counts after windowing.
        counts = new HashMap<String, Integer>();
    }
}
