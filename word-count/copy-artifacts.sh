#!/usr/bin/env bash

basedir=$(dirname $0)

#scp -i ~/Downloads/msc-hadoop.pem \
#$basedir/populate-file.sh \
#$basedir/mapreduce/run-local.sh \
#$basedir/mapreduce/target/scala-2.11/wordcount-mapreduce.jar \
#$basedir/spark-core/target/scala-2.11/wordcount-spark-batch_2.11-0.1.0.jar \
#$basedir/spark-core/run-local.sh \
#$basedir/flink-batch/target/scala-2.11/wordcount-flink-batch-assembly-0.1-SNAPSHOT.jar \
#hadoop@ec2-54-154-55-171.eu-west-1.compute.amazonaws.com:~/artifacts
#
scp -i ~/Downloads/msc-hadoop.pem \
$basedir/mapreduce/target/scala-2.11/wordcount-mapreduce.jar \
$basedir/spark-core/target/scala-2.11/wordcount-spark-batch_2.11-0.1.0.jar \
$basedir/flink-batch/target/scala-2.11/wordcount-flink-batch-assembly-0.1-SNAPSHOT.jar \
hadoop@ec2-54-154-55-171.eu-west-1.compute.amazonaws.com:~/artifacts

