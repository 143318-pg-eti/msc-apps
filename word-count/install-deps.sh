#!/usr/bin/env bash

set -xv

SCRIPT_LOC=`pwd`

DEPS_PATH="$SCRIPT_LOC/dependencies"
mkdir -p $DEPS_PATH

hadoop_release="hadoop-2.7.3"
export HADOOP_HOME=$DEPS_PATH/$hadoop_release
if [ ! -e $HADOOP_HOME ]; then
    wget "http://ftp.ps.pl/pub/apache/hadoop/common/$hadoop_release/$hadoop_release.tar.gz"
    tar -xzvf $hadoop_release.tar.gz -C $DEPS_PATH
    rm $hadoop_release.tar.gz
fi

spark_release="spark-2.1.0-bin-hadoop2.7"
export SPARK_HOME=$DEPS_PATH/$spark_release
if [ ! -e $SPARK_HOME ]; then
    wget "http://d3kbcqa49mib13.cloudfront.net/$spark_release.tgz"
    tar -xzvf $spark_release.tgz -C $DEPS_PATH
    rm $spark_release.tgz
fi

flink_release="flink-1.2.0"
flink_release_full="$flink_release-bin-hadoop27-scala_2.11"
export FLINK_HOME=$DEPS_PATH/$flink_release
if [ ! -e $FLINK_HOME ]; then
    wget "http://ftp.piotrkosoft.net/pub/mirrors/ftp.apache.org/flink/$flink_release/$flink_release_full.tgz"
    tar -xzvf $flink_release_full.tgz -C $DEPS_PATH
    rm $flink_release_full.tgz
fi

kafka_version="0.10.1.1"
kafka_release="kafka_2.11-$kafka_version"
export KAFKA_HOME=$DEPS_PATH/$kafka_release
if [ ! -e $KAFKA_HOME ]; then
    wget "http://ftp.piotrkosoft.net/pub/mirrors/ftp.apache.org/kafka/$kafka_version/$kafka_release.tgz"
    tar -xzvf $kafka_release.tgz -C $DEPS_PATH
    rm $kafka_release.tgz
fi

gearpump_version="0.8.2-incubating"
gearpump_release="gearpump_2.11-$gearpump_version"
export GEARPUMP_HOME=$DEPS_PATH/$gearpump_release
if [ ! -e $GEARPUMP_HOME ]; then
    wget "https://dist.apache.org/repos/dist/release/incubator/gearpump/0.8.2-incubating/$gearpump_release-bin.tgz"
    tar -xzvf $gearpump_release-bin.tgz -C $DEPS_PATH
    rm $gearpump_release-bin.tgz
fi

storm_release="apache-storm-1.0.3"
export STORM_HOME=$DEPS_PATH/$storm_release
if [ ! -e $STORM_HOME ]; then
    wget "http://www-eu.apache.org/dist/storm/$storm_release/$storm_release.tar.gz"
    tar -xzvf ${storm_release}.tar.gz -C $DEPS_PATH
    rm ${storm_release}.tar.gz
fi

apex_release="apache-apex-core-3.5.0"
export APEX_HOME=$DEPS_PATH/$apex_release
if [ ! -e $APEX_HOME ]; then
    wget "http://ftp.ps.pl/pub/apache/apex/$apex_release/$apex_release-source-release.tar.gz"
    tar -xzvf $apex_release-source-release.tar.gz -C $DEPS_PATH
    rm $apex_release-source-release.tar.gz
    pushd $APEX_HOME
    mvn clean install -DskipTests
    popd
fi


malhar_release="apache-apex-malhar-3.6.0"
export MALHAR_HOME=$DEPS_PATH/$malhar_release
if [ ! -e $MALHAR_HOME ]; then
    wget "http://ftp.ps.pl/pub/apache/apex/$malhar_release/$malhar_release-source-release.tar.gz"
    tar -xzvf $malhar_release-source-release.tar.gz -C $DEPS_PATH
    rm $malhar_release-source-release.tar.gz
    pushd $MALHAR_HOME
    mvn clean install -DskipTests
    popd
fi