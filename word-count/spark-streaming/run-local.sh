#!/usr/bin/env bash
set -xuve

echo "Required variables"
echo SPARK_HOME=${SPARK_HOME}
echo INPUT_TOPIC=${INPUT_TOPIC}
echo OUTPUT_TOPIC=${OUTPUT_TOPIC}
echo BOOTSTRAP_SERVERS=${BOOTSTRAP_SERVERS}


SCRIPT_LOC=`dirname $0`

${SPARK_HOME}/bin/spark-submit \
    --master "local[*]" \
    --class Main \
    ${SCRIPT_LOC}/target/scala-2.11/wordcount-spark-streaming-assembly-0.1.0.jar \
    ${INPUT_TOPIC} ${OUTPUT_TOPIC} ${BOOTSTRAP_SERVERS}
