

import java.util.{Properties, UUID}

import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.apache.kafka.common.serialization.{StringDeserializer, StringSerializer}
import org.apache.spark._
import org.apache.spark.streaming._
import org.apache.spark.streaming.kafka010.{ConsumerStrategies, KafkaUtils, LocationStrategies}

object Main {
  def main(args: Array[String]) {

    val conf = new SparkConf().setMaster("local[*]").setAppName("word-count spark-streaming")
    val ssc = new StreamingContext(conf, Seconds(5))


    val inputStreamName = args(0)
    val outputStreamName = args(1)
    val bootstrapServers = args(2)

    val kafkaParams = Map[String, Object](
      "bootstrap.servers" -> bootstrapServers,
      "key.deserializer" -> classOf[StringDeserializer],
      "value.deserializer" -> classOf[StringDeserializer],
      "auto.offset.reset" -> "earliest",
      "enable.auto.commit" -> (false: java.lang.Boolean),
      "group.id" -> s"${UUID.randomUUID().toString}"
    )
    val stream = KafkaUtils.createDirectStream[String, String](
      ssc,
      LocationStrategies.PreferConsistent,
      ConsumerStrategies.Subscribe[String, String](Array(inputStreamName), kafkaParams)
    )

    val wordCounts = stream.flatMap(r => r.value().split("[\\s]+").filter(_.nonEmpty))
      .map(word => (word, 1))
      .reduceByKey(_ + _)

    val producerConfig: Properties = {
      val p = new Properties()
      p.setProperty("bootstrap.servers", bootstrapServers)
      p.setProperty("key.serializer", classOf[StringSerializer].getName)
      p.setProperty("value.serializer", classOf[StringSerializer].getName)
      p
    }

    wordCounts.foreachRDD { rdd =>
      val counts = rdd.collect().sortBy(_._2).reverse
      val producer = new KafkaProducer[String, String](producerConfig)
      counts.foreach(r =>
        producer.send(new ProducerRecord[String, String](outputStreamName, "", r._1 + " " + r._2.toString))
      )
    }

    ssc.start()
    ssc.awaitTerminationOrTimeout(10000)
  }
}