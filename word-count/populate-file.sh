#!/usr/bin/env bash

set -uxe

file=$1
output_dir=$2
num_copies=$3

for i in $(seq 1 $num_copies); do
    hdfs dfs -cp -f $file $output_dir/$(basename $file)-$i
done