import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.kafka.*;
import org.apache.storm.kafka.bolt.KafkaBolt;
import org.apache.storm.kafka.bolt.mapper.FieldNameBasedTupleToKafkaMapper;
import org.apache.storm.kafka.bolt.mapper.TupleToKafkaMapper;
import org.apache.storm.kafka.bolt.selector.DefaultTopicSelector;
import org.apache.storm.spout.SchemeAsMultiScheme;
import org.apache.storm.topology.BasicOutputCollector;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.TopologyBuilder;
import org.apache.storm.topology.base.BaseBasicBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

import java.util.*;

public class WordCountTopology {

    public static void main(String[] args) throws Exception {


        String inputTopic = args[0];
        String outputTopic = args[1];
        String bootstrapServers = args[2];
        BrokerHosts hosts = new ZkHosts("localhost");
        SpoutConfig spoutConfig = new SpoutConfig(hosts, inputTopic, "/input", UUID.randomUUID().toString());
        spoutConfig.scheme = new SchemeAsMultiScheme(new StringScheme());
        KafkaSpout kafkaSpout = new KafkaSpout(spoutConfig);

        TopologyBuilder builder = new TopologyBuilder();
        builder.setSpout("spout", kafkaSpout, 5);
        builder.setBolt("split", new SplitSentence(), 8)
                .shuffleGrouping("spout");
        builder.setBolt("count", new WordCount(), 12)
                .fieldsGrouping("split", new Fields("word"));

        Properties props = new Properties();
        props.put("bootstrap.servers", bootstrapServers);
        props.put("serializer.class", "kafka.serializer.StringEncoder");
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

        KafkaBolt kafkaBolt = new KafkaBolt()
                .withTopicSelector(new DefaultTopicSelector(outputTopic))
                .withTupleToKafkaMapper(new ToKafkaMapper())
                .withProducerProperties(props);
        builder.setBolt("forwardToKafka", kafkaBolt, 8).shuffleGrouping("count");

        Config conf = new Config();


//            StormSubmitter.submitTopologyWithProgressBar(args[0], conf, builder.createTopology());

        LocalCluster cluster = new LocalCluster();
        cluster.submitTopology("word-count", conf, builder.createTopology());

        Thread.sleep(5000);

        cluster.shutdown();

    }

    public static class SplitSentence extends BaseBasicBolt {

        @Override
        public void execute(Tuple input, BasicOutputCollector collector) {
            String[] split = input.getString(0).split("[\\s]+");
            for (String word : split) {
                collector.emit(Arrays.asList(word));
            }
        }

        @Override
        public void declareOutputFields(OutputFieldsDeclarer declarer) {
            declarer.declare(new Fields("word"));
        }
    }

    public static class WordCount extends BaseBasicBolt {
        Map<String, Integer> counts = new HashMap<String, Integer>();

        @Override
        public void execute(Tuple tuple, BasicOutputCollector collector) {
            String word = tuple.getString(0);
            Integer count = counts.get(word);
            if (count == null)
                count = 0;
            count++;
            counts.put(word, count);
            collector.emit(new Values(word, count));
        }

        @Override
        public void declareOutputFields(OutputFieldsDeclarer declarer) {
            declarer.declare(new Fields("word", "count"));
        }
    }

    public static class ToKafkaMapper implements TupleToKafkaMapper<String, String> {

        @Override
        public String getKeyFromTuple(Tuple tuple) {
            return "";
        }

        @Override
        public String getMessageFromTuple(Tuple tuple) {
            return tuple.getString(0) + " " + tuple.getInteger(1).toString();
        }
    }

}