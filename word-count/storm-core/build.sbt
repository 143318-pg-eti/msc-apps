name := "wordcount-storm-core"

scalaVersion := "2.11.7"

libraryDependencies += "org.apache.storm" %  "storm-core" % "1.0.3" % "provided"
libraryDependencies += "org.apache.storm" %  "storm-kafka" % "1.0.3" % "provided"

libraryDependencies += "org.apache.storm" %  "storm-core" % "1.0.3"
libraryDependencies += "org.apache.storm" %  "storm-kafka" % "1.0.3"
libraryDependencies += "org.apache.kafka" % "kafka_2.11" % "0.10.2.0" exclude ("org.apache.zookeeper", "zookeeper") exclude ("log4j", "log4j") exclude("org.slf4j", "slf4j-log4j12")

assemblyMergeStrategy in assembly := {
  case PathList("META-INF", "MANIFEST.MF") => MergeStrategy.discard
  case _ => MergeStrategy.first
}