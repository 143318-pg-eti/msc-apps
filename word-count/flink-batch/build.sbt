name := "wordcount-flink-batch"

scalaVersion := "2.11.7"

libraryDependencies += "org.apache.flink" %%  "flink-scala" % "1.2.0" % "provided"
libraryDependencies += "org.apache.flink" %% "flink-clients" % "1.2.0" % "provided"
libraryDependencies += "org.apache.flink" %% "flink-hadoop-compatibility" % "1.2.0"

//artifactName := { (sv: ScalaVersion, module: ModuleID, artifact: Artifact) =>
//  artifact.name + "." + artifact.extension
//}
