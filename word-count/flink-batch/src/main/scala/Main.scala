import org.apache.flink.api.common.operators.Order
import org.apache.flink.api.common.typeinfo.TypeInformation
import org.apache.flink.api.scala._
import org.apache.flink.api.scala.hadoop.mapreduce.HadoopOutputFormat
import org.apache.hadoop.fs.Path
import org.apache.hadoop.io.{IntWritable, Text}
import org.apache.hadoop.mapreduce.Job
import org.apache.hadoop.mapreduce.lib.output.{FileOutputFormat, TextOutputFormat}

import scala.reflect.ClassTag


object Main {
  def main(args: Array[String]) {

    // set up the execution environment
    val env = ExecutionEnvironment.getExecutionEnvironment

    implicit val outputTypeInfo = createOutputTypeInformation()

    // get input data
    val text = env.readTextFile(args(0))
    val counts = text.flatMap { _.toLowerCase.split("\\W+") }
      .map { (_, 1) }
      .groupBy(0)
      .sum(1)
//      .sortPartition(_._2, Order.DESCENDING)
      .map{ keyValue => (new Text(keyValue._1), new IntWritable(keyValue._2)) }(outputTypeInfo, implicitly[ClassTag[(Text, IntWritable)]])

    // Set up the Hadoop TextOutputFormat.
    val job = Job.getInstance
    val hadoopOF = new HadoopOutputFormat[Text, IntWritable](new TextOutputFormat[Text, IntWritable], job)
    hadoopOF.getConfiguration.set(TextOutputFormat.SEPERATOR, " ")
    FileOutputFormat.setOutputPath(job, new Path(args(1)))
    counts.output(hadoopOF)

    env.execute()

    System.exit(0)
  }

  // workaround for problems with flink support for hadoop.Wirtiable typeinformation
  // following line fails for unknown reason
  // val z = Class.forName("org.apache.flink.api.java.typeutils.WritableTypeInfo", false, classOf[TypeExtractor].getClassLoader)
  def createOutputTypeInformation(): TypeInformation[(Text, IntWritable)] = {
    val intWritableTypeInfo = new org.apache.flink.api.java.typeutils.WritableTypeInfo(classOf[IntWritable])
    val textTypeInfo = new org.apache.flink.api.java.typeutils.WritableTypeInfo(classOf[Text])
    createTuple2TypeInformation(textTypeInfo, intWritableTypeInfo)
  }
}