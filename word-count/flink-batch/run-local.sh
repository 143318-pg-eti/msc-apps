#!/usr/bin/env bash
set -xuve

echo "Required variables"
echo FLINK_HOME=${FLINK_HOME}
echo INPUT_PATH=${INPUT_PATH}
echo OUTPUT_PATH=${OUTPUT_PATH}


SCRIPT_LOC=`dirname $0`

${FLINK_HOME}/bin/start-local.sh &

${FLINK_HOME}/bin/flink run \
    -c Main \
    ${SCRIPT_LOC}/target/scala-2.11/wordcount-flink-batch-assembly-0.1-SNAPSHOT.jar \
    ${INPUT_PATH} ${OUTPUT_PATH}

${FLINK_HOME}/bin/stop-local.sh


