package com.example.myapexapp;

import com.datatorrent.api.Sink;

/**
 * Created by wpitula on 4/2/17.
 */
public class RandomNumberGeneratorTest {

    public static void main(String[] args) {

        RandomNumberGenerator operator = new RandomNumberGenerator();

        operator.out.setSink(new Sink<Object>() {
            @Override
            public void put(Object tuple) {
                System.out.println(tuple);
            }

            @Override
            public int getCount(boolean reset) {
                return 0;
            }
        });

        operator.emitTuples();


    }
}
