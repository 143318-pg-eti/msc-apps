/**
 * Put your copyright and license info here.
 */
package com.example.myapexapp;

import org.apache.apex.malhar.kafka.KafkaSinglePortInputOperator;
//import org.apache.hadoop.conf.Configuration;

import com.datatorrent.api.annotation.ApplicationAnnotation;
import com.datatorrent.api.StreamingApplication;
import com.datatorrent.api.DAG;
import com.datatorrent.api.DAG.Locality;
import com.datatorrent.lib.io.ConsoleOutputOperator;

@ApplicationAnnotation(name="MyFirstApplication")
public class Application
{

//  @Override
//  public void populateDAG(DAG dag, Configuration conf)
//  {
//    // Sample DAG with 2 operators
//    // Replace this code with the DAG you want to build
//
//    KafkaSinglePortInputOperator input =  dag.addOperator("MessageReader", new KafkaSinglePortInputOperator());
//    ConsoleOutputOperator output = dag.addOperator("Output", new ConsoleOutputOperator());
//    dag.addStream("MessageData", input.outputPort, output.input);
//  }
}
