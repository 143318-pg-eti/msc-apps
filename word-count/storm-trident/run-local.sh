#!/usr/bin/env bash
set -xuve

echo "Required variables"
echo STORM_HOME=${STORM_HOME}
echo INPUT_TOPIC =${INPUT_TOPIC}
echo OUTPUT_TOPIC =${OUTPUT_TOPIC}

SCRIPT_LOC=`dirname $0`

java -cp ${SCRIPT_LOC}/target/scala-2.11/wordcount-storm-trident-assembly-0.1-SNAPSHOT.jar WordCountTopology \
    ${INPUT_TOPIC} ${OUTPUT_TOPIC} ${BOOTSTRAP_SERVERS} &

sleep 4
echo "STOP" | $KAFKA_HOME/bin/kafka-console-producer.sh --broker-list $BOOTSTRAP_SERVERS --topic $INPUT_TOPIC
sleep 4
kill %1

